<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\Rover;
use \App\Model\Heading;
use \App\Model\Plateau;
use \App\Model\Position;
use \App\Interfaces\iInstruction;
use \App\Model\Instruction;

final class Scenario1Test extends TestCase
{

    public function testScenario1(): void
    {
        $rover = new Rover(new Plateau(5, 5), new Position(1, 2), new Heading(Heading::HEADING_NORTH));
        $rover->action(new Instruction(iInstruction::ACTION_TURN_LEFT));
        $rover->action(new Instruction(iInstruction::ACTION_MOVE));
        $rover->action(new Instruction(iInstruction::ACTION_TURN_LEFT));
        $rover->action(new Instruction(iInstruction::ACTION_MOVE));
        $rover->action(new Instruction(iInstruction::ACTION_TURN_LEFT));
        $rover->action(new Instruction(iInstruction::ACTION_MOVE));
        $rover->action(new Instruction(iInstruction::ACTION_TURN_LEFT));
        $rover->action(new Instruction(iInstruction::ACTION_MOVE));
        $rover->action(new Instruction(iInstruction::ACTION_MOVE));


        $this->assertSame(1, $rover->getPosition()->getX());
        $this->assertSame(3, $rover->getPosition()->getY());
        $this->assertSame(Heading::HEADING_NORTH, $rover->getHeading()->asString());
    }

}
