<?php

namespace App\Model;


use App\Interfaces\iHeading;
use App\Interfaces\iInstruction;
use App\Interfaces\iPlateau;
use App\Interfaces\iPosition;
use App\Interfaces\iRover;

class Rover implements iRover
{
    public function __construct(iPlateau $plateau, iPosition $position, iHeading $heading)
    {
        // TODO: Implement __construct() method.
    }

    public function getPosition(): iPosition
    {
        // TODO: Implement getPosition() method.
    }

    public function getHeading(): iHeading
    {
        // TODO: Implement getHeading() method.
    }

    function action(iInstruction $Instruction)
    {
        // TODO: Implement action() method.
    }

}
